#!/usr/bin/env python

import rospy
import os
import rosbag
from sensor_msgs.msg import JointState
import sys
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive

gauth = GoogleAuth()
gauth.LocalWebserverAuth()
drive = GoogleDrive(gauth)
gfile = drive.CreateFile({'parents': [{'id': '1w8xpVnIbZWDGENeIcaWlOE2-Ie0y0x68'}]}) #pepper_bagfiles folder ID in google drive

#name the bagfile and create bag
txt = raw_input("name the bagfile: ")
path = txt+".bag"
bag = rosbag.Bag(path,'w')

def callback(data):
	#save data in the bag
    bag.write('State',data)


#suscribe to joint_states topic and continously save it in the bag
def main():
	rospy.init_node('listener')
	rospy.Subscriber("/joint_states", JointState, callback)

	rate = rospy.Rate(1) # 10hz
	while not rospy.is_shutdown():
		rate.sleep()
	else:
		bag.close()
		#upload bag to google drive and remove it locally
		gfile.SetContentFile(path)
		gfile.Upload()
		os.remove(path)

if __name__ == '__main__':
    main()