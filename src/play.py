#!/usr/bin/env python

import os
import rosbag
import rospy
import sys
import copy
import moveit_commander
import pandas as pd
from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive


#-------------- methods ----------------

#identify limb depending on chosen joints
def get_limb(index):
    if index == ['RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll', 'RWristYaw']:
        return "RIGHT_ARM"
    elif index == ['RHand']:
        return "RIGHT_HAND"


#add point to the list
def add_point(point, index):
    point_pos = [0]*len(index)
    for x in range(len(index)):
        point_pos[x] = point.position[index[x]]
    return point_pos

#get numerical indices of the values that we care about from rosbag
#this varies if the movements were recorded from moveit interface
#or were recorded from teleport, as JointState message variables change
#(see report for further info)
def get_index(index):
    num_index = []
    for topic, data, t in bag.read_messages():
        names = data.name # e.g. ['HeadPitch', 'HeadYaw', 'HipPitch', 'HipRoll', 'KneePitch', 'LElbowRoll', 'LElbowYaw', 'LHand', 'LShoulderPitch', 'LShoulderRoll', 'LWristYaw', 'RElbowRoll', 'RElbowYaw', 'RHand', 'RShoulderPitch', 'RShoulderRoll', 'RWristYaw']
        for x in index: #e.g. we only want ['RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll', 'RWristYaw'] indices to get the point's values
            count = 0
            for y in names: 
                if x == y:
                    num_index.append(count)
                count += 1
        break
    return num_index

#extract starting point, middle points and ending point from rosbag
def extract_points(index):

    limb = get_limb(index)
    index = get_index(index)
    goals = [[],[],[]]
    count = 0

    if "ARM" in limb:

        #set increase and prev_increase to false, as no joint direction change in the beginning
        #set position to 0
        increase = [False]*len(index)
        prev_increase =  [False]*len(index)
        position = [0]*len(index)
        prev_position = [0]*len(index)

        for topic, data, t in bag.read_messages():
            
            #add starting point
            if count == 0:
                goals[0].append(add_point(data, index))
                goals[1].append(data.header.seq) #similar to time stamp
                goals[2].append(limb)

            #add ending point
            elif count == size:
                goals[0].append(add_point(data, index))
                goals[1].append(data.header.seq)
                goals[2].append(limb)

            #add middle points (by analysing increase/deacrease variation) 
            else:
                for x in range(len(index)):
                    position[x] = data.position[index[x]]

                    #check the direction of each of the joints
                    #joint value increasing
                    if position[x] > prev_position[x]:
                        increase[x] = True
                    #joint value decreasing
                    else:
                        increase[x] = False

                #if direction changes in any of the joints, save that point
                #(e.g. if a joint value was increasing and sudenly starts deacreasing)
                #it means we reached a "middle point"
                if increase != prev_increase:
                    goals[0].append(add_point(data, index))
                    goals[1].append(data.header.seq)
                    goals[2].append(limb)
                
                #update prev_ variables
                #use copy.copy otherwise e.g. prev_increase will point to increase (only happens with arrays?)
                prev_increase = copy.copy(increase)
                prev_position = copy.copy(position)

            count += 1

        #remove duplicate or similar points
        goals = filter(goals,index)
            

    elif "HAND" in limb:

        count2 = 0
        prev_pos = 0
        last_saved_pos = 0
        index_val = index[0] #e.g. 16

        for topic, data, t in bag.read_messages():
            
            # add starting point
            if count == 0:
                goals[0].append(add_point(data, index))
                goals[1].append(data.header.seq)
                goals[2].append(limb)
                prev_pos = data.position[index_val] 
                last_saved_pos = data.position[index_val]

            # add middle points and ending point 
            else:
                pos = data.position[index_val]

                #if one hand position has been done long enough its a goal and not a transition state 
                #the threshold determines what is "long enough" in this case
                if pos == prev_pos:
                    count2 += 1
                    if count2 >= hand_threshold:
                        count2 = 0
                        #if this point/goal has not already being saved, then add it
                        if pos != last_saved_pos:
                            goals[0].append(add_point(data, index))
                            goals[1].append(data.header.seq)
                            goals[2].append(limb)
                            last_saved_pos = pos

                prev_pos = pos
            
            count += 1
    
    return goals

 #filter duplicate or similar points by compering the difference with previous point
def filter(points, index):

    goals = [[],[],[]] #list to save the filtered points
    prev_x = [100]*len(index) #large value so that the starting point is saved
    count = 0
    diff = 0

    for x in points[0]:

        for a in range(len(index)):
            diff = prev_x[a] - x[a]
            #if difference of joints position with respect to previous one is big enough (depenpend on threshold)
            #then save point
            if diff <= -arm_threshold or diff >= arm_threshold: # between 0.0 and 1.0: the smaller the number, more accurate but less efficient
                goals[0].append(x)
                goals[1].append(points[1][count])
                goals[2].append(points[2][count])
                break
            
        prev_x = copy.copy(x)
        count += 1

    return goals

#taking into account data.header.seq (basically a timestamp), make a sequence of different
#limb movements, as moveit can't execute two parallel movements (use to different group commanders) at the same time
#so we have to merge all the points and reorder them using header.seq as reference row (from lower to higher)
def planner(a,b):
    
    all_goals = [a[i]+b[i] for i in xrange(len(a))] #merge goal arrays (arm and hand)
    time_row = all_goals[1]

    #add indices
    reference_row = [(idx, value) for (idx, value) in enumerate(time_row)]
    ref_row_sorted = sorted(reference_row, key=lambda x: x[1])
    #extract indices
    indices = [x[0] for x in ref_row_sorted]

    ordered_goals = []
    for row in all_goals:
        new_row = [row[x] for x in indices]
        ordered_goals.append(new_row)

    print(ordered_goals[2])
    return ordered_goals

#for pepper's moveit "right hand" commander to work, each joint_state (point) has to be composed by:
#['RFinger11', 'RFinger12', 'RFinger13', 'RFinger21','RFinger22', 'RFinger23', 'RFinger31', 
#'RFinger32', 'RFinger33', 'RFinger41', 'RFinger42', 'RFinger43', 'RHand', 'RThumb1', 'RThumb2']
#as we only get RHand from recording from teleport, we will manually calculate the rest using a constant (explained in the report)
def get_hand_goal(joint_goal):
    finger_val = joint_goal[0]*0.87 #all the 14 new elements will have the same value
    hand_goal = [0]*15
    for x in range(len(hand_goal)):
        if x == 12: #this is where RHand value needs to be in the array
            hand_goal[x] = joint_goal[0]
        else:
            hand_goal[x] = finger_val
    return hand_goal


#for future work: in case you want to save the filtered values to start with the generalization 
#(example of how to save in google drive as csv file)
def create_csv(dataset):
    gfile = drive.CreateFile({'parents': [{'id': '1w8xpVnIbZWDGENeIcaWlOE2-Ie0y0x68'}]})
    p = 'sample.csv'
    pd.DataFrame(dataset).to_txt(p)
    gfile.SetContentFile(p)
    gfile.Upload()
    os.remove(p)


#---------------------------------------------------

gauth = GoogleAuth()
gauth.LocalWebserverAuth()
drive = GoogleDrive(gauth)

#choose rosbag from google drive pepper_rosbag folder
files = []
file_list = drive.ListFile({'q': "'{}' in parents and trashed=false".format('1w8xpVnIbZWDGENeIcaWlOE2-Ie0y0x68')}).GetList() #get file list
for file in file_list:
    files.append(file['title']) #get filenames
print(files) #print filenames
path = raw_input("pick a rosbag from the list above: ")

#download and open selected rosbag
for file in file_list:
    if file['title'] == path:
        print('title: %s, id: %s' % (file['title'],file['id']))
        file.GetContentFile(file['title']) #downloads from google drive

#define variables
bag = rosbag.Bag(path)
arm_threshold = float(raw_input("give a threshold value for the right arm: ")) #between 0.0 and 1.0: the smaller the number, more detailed
hand_threshold = float(raw_input("give a threshold value for the right hand: ")) #usually set to 10
pick_limbs = raw_input("choose to replay 'arm', 'hand' or 'both': ")
size = bag.get_message_count()
#needed variables for moveit_commander.MoveGroupCommander("right_arm")
right_arm_index = ['RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll', 'RWristYaw']
#needed variable for moveit_commander.MoveGroupCommander("right_hand") -> we will need to add other 14 elements to the array later
right_hand_index = ['RHand'] 


#process depending 'arm', 'hand' or 'both' was chosen
#extract the important JointState states (which I call them points) from the rosbag
#different methods of filtering for arm and hand
#plan sequence if both are chosen
if pick_limbs == 'arm':
    uniq = extract_points(right_arm_index)
    bag.close()
    size_uniq = len(uniq[0])

elif pick_limbs == 'hand':
    uniq = extract_points(right_hand_index)
    bag.close()
    size_uniq = len(uniq[0])

elif pick_limbs == 'both':
    arm_uniq = extract_points(right_arm_index)
    hand_uniq = extract_points(right_hand_index)
    bag.close()
    uniq = planner(arm_uniq,hand_uniq)
    size_uniq = len(uniq[0])
    print(uniq[2]) #print sequence/plan


#initialize  node and moveit commander
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("current_pos", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group = moveit_commander.MoveGroupCommander("right_arm")
hand = moveit_commander.MoveGroupCommander("right_hand")

#go to all points
for x in range(size_uniq):

    joint_goal = uniq[0][x] #get point
    goal_limb = uniq[2][x] #get limb
    print("moving "+ goal_limb)

    #choose commander depending on what limb is next in the sequence
    if "RIGHT_ARM" == goal_limb:
        group.go(joint_goal, wait=True)
        group.stop()
        group.clear_pose_targets()
        print("movement %d completed!" %x)

    if "RIGHT_HAND" == goal_limb:
        #we have a 1 element array and we need a 15 elements 
        #array as joint_goal for the MoveGroupCommander("right_hand")
        #see get_hand_goal(joint_goal) method explanation
        joint_goal = get_hand_goal(joint_goal)
        uniq[0][x] = joint_goal #save the new array
        hand.go(joint_goal, wait=True)
        hand.stop()
        hand.clear_pose_targets()
        print("movement %d completed!" %x)


#create_csv(uniq[0])

#delete downloaded rosbag
os.remove(path)
